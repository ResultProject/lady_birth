# Lady Birth

Программа собирает анкеты, анализирует когда у них день рождения, 
собирает подарки и отображает. 

### Функции

* Просмотреть, когда день рождение у клиентки. 
* Посмотреть был ли доставлен подарок на день рождение клиентки. 
* Добавлять напоминание в гугл календарь. 

### Запуск

Запускается как обычная программа. 

Параметры:
* --before {num} - начать анализ подарков за {num} дней до дня рождения. 
* --after {num} - закончить анализ подарков спустя {num} дней после дня рождения. 

Запуск с параметрами:

```java -jar /path/to/app.jar --before {num} --after {num}```

### Принцип работы

1. Собирает анкеты и данные о днях рождения 
2. Фильтрует анкеты у кого будет др в ближайшие 30 дней и у кого было в за 30 дней 
3. По отфильтрованным анкетам собирает данные о подарках исходя из входящих параметров
--before и --after по умолчанию before=5 дней, a after=6 дней. То есть за 5 дней до 
дня рождения и 6 дней после дня рождения 
4. Отображает данные 
5. Есть четыре типа иконок: перечеркнутая - анкета не может получать подарки, назад - 
доставка провалена, грузовик - доставка в процессе, подарок - подарок доставлен 
6. Количество иконок равносильно количеству подарков 
7. В подробной информации открывается диалог, в котором есть инфа о клиентке и о подарке 
включая имя подарка и сумму
8. Расшифровать operatorEmail невозможно так как с запросом на /edit-female нет 
атрибутов cf-email, соответственно и данных, которые можно декодировать

### История версий

##### v0.4

* Количество иконок = количество подарков 
* Загрузка всех типов подарков (не зависимо от статуса) 
* Добавлены статусы к подаркам + иконки: грузовик (в процессе) и назад (провалена) 
* Добавлено отображение содержания подарка в диалоге 
* Добавлена загрузка до n и после n дней около дня рождения 
* Добавлена возможность изменить параметры загрузки до n и после n дней 
около дня рождения, используя параметры запуска --before и --after 
* Оптимизация ресурсов многопоточности 