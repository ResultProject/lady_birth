package com.result.lady.birth.pros

import com.result.lady.birth.service.AdminPanelStorageService
import com.result.lady.birth.ui.controller.ProfilesListWindowController
import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.control.Alert
import javafx.scene.layout.Pane
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import javafx.stage.Stage


class Main : Application() {
	override fun start(primaryStage: Stage?) {
		val panels = AdminPanelStorageService.restore()

		try {
			if (panels.isEmpty()) {
//                WindowFactory.showSettingsWindow(primaryStage!!)


				val stage = Stage()
				val loader = FXMLLoader(javaClass.getResource("/fxml/settingsView.fxml"))
				val root = loader.load<VBox>()
				stage.scene = Scene(root, 400.0, 600.0)
				SizeBinder.registerScene(stage.scene)

				stage.show()
			} else {
//                WindowFactory.showProfilesListWindow(primaryStage!!, panels)
				val stage = Stage()
				val loader = FXMLLoader(javaClass.getResource("/fxml/profilesListView.fxml"))
				val root = loader.load<StackPane>()
				stage.scene = Scene(root, 400.0, 600.0)
				SizeBinder.registerScene(stage.scene)

				stage.show()

				val controller = loader.getController<ProfilesListWindowController>()
				controller.loadData()
			}
		} catch (e: Exception) {
			e.printStackTrace()


			val alert = Alert(Alert.AlertType.ERROR)
			alert.title = "Error Dialog"
			alert.headerText = "Look, an Error Dialog"
			alert.contentText = "Ooops, there was an error!"

			alert.showAndWait()
		}
	}
}

object SizeBinder {
	private var nodes = mutableListOf<Pane>()

	fun add(node: Pane) {
		nodes.add(node)
	}

	fun registerScene(scene: Scene) {
		scene.heightProperty().addListener { observable, oldValue, newValue -> nodes.forEach { it.prefHeight = newValue.toDouble() } }
		scene.widthProperty().addListener { observable, oldValue, newValue -> nodes.forEach { it.prefWidth = newValue.toDouble() } }
	}
}

private const val beforeParam = "--before"
private const val afterParam = "--after"

fun main(args: Array<String>) {

	if (args.contains(beforeParam)) {
		val index = args.indexOf(beforeParam) + 1
		com.result.lady.birth.Config.beforeDay = args[index]
	} else {
		com.result.lady.birth.Config.beforeDay = "5"
	}

	if (args.contains(afterParam)) {
		val index = args.indexOf(afterParam)
		com.result.lady.birth.Config.afterDay = args[index]
	} else {
		com.result.lady.birth.Config.afterDay = "6"
	}

	Application.launch(Main::class.java)
}