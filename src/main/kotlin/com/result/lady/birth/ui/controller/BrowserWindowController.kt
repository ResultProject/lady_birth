package com.result.lady.birth.ui.controller

import javafx.fxml.FXML
import javafx.scene.web.WebView
import java.net.CookieHandler
import java.net.CookieManager


class BrowserWindowController : AbstractController() {

    @FXML
    lateinit var webView: WebView

    private lateinit var url: String

    private val cookieManager: CookieManager = CookieManager()

    fun loadPage(url: String) {
        this.url = url
        webView.engine.load(url)
        CookieHandler.setDefault(cookieManager)
    }

    fun setCookies() {}
}