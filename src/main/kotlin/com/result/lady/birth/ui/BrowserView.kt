package com.result.lady.birth.ui

import com.jfoenix.controls.JFXButton
import com.result.lady.birth.ui.util.AnchorPaneUtil
import javafx.application.Platform
import javafx.scene.Node
import javafx.scene.layout.AnchorPane
import javafx.scene.web.WebView
import java.net.CookieHandler
import java.net.CookieManager


class BrowserView(url: String, val cookieManager: CookieManager) {

    private val webView = WebView()

    private val nextButton = JFXButton("Вперед")

    private val backButton = JFXButton("Назад")

    init {
        setAnchor()
        setStyle()
        setListeners()
        webView.engine.load(url)
        CookieHandler.setDefault(cookieManager)
    }

    fun getElements(): Array<Node> {
        return arrayOf(webView, nextButton, backButton)
    }

    fun setAnchor() {
        AnchorPane.setTopAnchor(nextButton, 5.0)
        AnchorPane.setLeftAnchor(nextButton, 70.0)

        AnchorPane.setTopAnchor(backButton, 5.0)
        AnchorPane.setLeftAnchor(backButton, 5.0)

        AnchorPaneUtil.setPosition(webView, 40.0, 0.0, 0.0, 0.0)
    }

    fun setStyle() {
        nextButton.style = "-fx-background-color: #eeeeee;"
        backButton.style = "-fx-background-color: #eeeeee;"
    }

    fun setListeners() {
        backButton.setOnAction { goBackHistory() }
        nextButton.setOnAction { goForwardHistory() }
    }

    fun goBackHistory(): String {
        val history = webView.engine.history
        val entryList = history.entries
        val currentIndex = history.currentIndex

        Platform.runLater({ history.go(-1) })
        return entryList[if (currentIndex > 0) currentIndex - 1 else currentIndex].url
    }

    fun goForwardHistory(): String {
        val history = webView.engine.history
        val entryList = history.entries
        val currentIndex = history.currentIndex

        Platform.runLater({ history.go(1) })
        return entryList[if (currentIndex < entryList.size - 1) currentIndex + 1 else currentIndex].url
    }
}