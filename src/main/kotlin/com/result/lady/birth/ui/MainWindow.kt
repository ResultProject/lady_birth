package com.result.lady.birth.ui

import com.result.lady.birth.Config
import com.result.lady.birth.service.AdminPanelStorageService
import com.result.lady.birth.ui.util.WindowFactory
import javafx.application.Application
import javafx.scene.control.Alert
import javafx.scene.control.Alert.AlertType
import javafx.stage.Stage


class MainWindow : Application() {
	override fun start(primaryStage: Stage?) {
		// TODO: refactor owner stage
		val panels = AdminPanelStorageService.restore()

		try {
			if (panels.isEmpty()) {
				println("ANA")
				WindowFactory.showSettingsWindow(primaryStage!!)
			} else {
				WindowFactory.showProfilesListWindow(primaryStage!!, panels)
			}

		} catch (e: Exception) {
			val alert = Alert(AlertType.ERROR)
			alert.title = "Ошибка"
			alert.headerText = "Случилась непредвиденная ошибка"
			alert.contentText = "Перезапустите приложение или обратитесь к разработчикам приложения"

			alert.showAndWait()
		}
	}

	companion object {
		private const val beforeParam = "--before"
		private const val afterParam = "--after"

		@JvmStatic
		fun main(args: Array<String>) {
			if (args.contains(beforeParam)) {
				val index = args.indexOf(beforeParam) + 1
				Config.beforeDay = args[index]
			} else {
				Config.beforeDay = "5"
			}

			if (args.contains(afterParam)) {
				val index = args.indexOf(afterParam)
				Config.afterDay = args[index]
			} else {
				Config.afterDay = "6"
			}

			launch(MainWindow::class.java)
		}
	}
}