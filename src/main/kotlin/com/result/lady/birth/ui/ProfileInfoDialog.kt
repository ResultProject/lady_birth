package com.result.lady.birth.ui

import com.jfoenix.controls.*
import com.result.lady.birth.model.Gift
import com.result.lady.birth.model.GiftItem
import com.result.lady.birth.model.Profile
import com.result.lady.birth.model.RecursiveGiftItem
import com.result.lady.birth.service.AuthService
import com.result.lady.birth.service.ClientManager
import javafx.application.Platform
import javafx.application.Platform.runLater
import javafx.beans.value.ObservableValue
import javafx.collections.FXCollections
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.ScrollPane
import javafx.scene.control.TextField
import javafx.scene.layout.HBox
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import javafx.scene.text.Font
import javafx.stage.Stage


class ProfileInfoDialog(profile: Profile, rootPane: StackPane) {
	private val EDIT_FEMALE = "${ClientManager.BASE_URL}/manager/edit-female/"

	private lateinit var profile: Profile

	val dialog: JFXDialog

	val dialogContainer: StackPane

	init {
		dialog = create(profile, rootPane)
		dialogContainer = dialog.dialogContainer
	}

	fun show() {
		dialog.show()
	}

	private fun create(profile: Profile, rootPane: StackPane): JFXDialog {

		this.profile = profile

		val content = JFXDialogLayout()
		content.setHeading(SelectableText(profile.name))

		val bodyText = createSelectableView(profile)

		val box = VBox(bodyText.box)
		if (profile.gifts.isNotEmpty()) {
			val giftsView = createGiftsView(profile)
			box.children.add(giftsView)
		}

		content.setBody(box)

		val dialog = JFXDialog(rootPane, content, JFXDialog.DialogTransition.CENTER)

		val buttons = createButtons(profile)
		content.setActions(*buttons)

		return dialog
	}

	private fun createSelectableView(profile: Profile): SelectableView {
		return SelectableView("ID = ${profile.id}",
				"Дата рождения = ${profile.birthday}",
				"Переводчик = ${profile.translatorEmail}",
				"Админка = ${profile.adminPanel!!.name}")
	}

	private fun createGiftsView(profile: Profile): ScrollPane {
		val vContainer = VBox().apply {
			spacing = 10.0
			padding = Insets(10.0)
		}

		if (profile.gifts.isNotEmpty()) {
			val giftViews = profile.gifts.map(::createGiftVBox)
			vContainer.children.addAll(giftViews)
		}

		return ScrollPane(vContainer).apply {
			isFitToWidth = true
			prefHeight = 400.0
		}
	}

	private fun createGiftVBox(gift: Gift): VBox {
		val sender = Label(gift.sender).apply {
			font = Font(18.0)
		}

		val headerBox = HBox(sender).apply {
			alignment = Pos.CENTER
		}

		val table = createGiftItemTable(gift.items)
		val itemsBox = VBox(table).apply {
			spacing = 5.0
			prefHeight = 120.0
		}

		/*gift.items.forEach {
			val itemContainer: HBox = HBox().apply {
				spacing = 10.0
			}

			with(it) {
				val nameLabel = Label(name)
				val amountLabel = Label(amount.toString())
				val priceLabel = Label(price.toString())
				val realPriceLabel = Label(realPrice.toString())

				itemContainer.children.addAll(nameLabel, amountLabel, priceLabel, realPriceLabel)
			}

			itemsBox.children.add(itemContainer)
			itemsBox.children.add(Separator())/
		}*/

		//val totalPrice = gift.items.sumBy { it.price }
		//val totalRealPrice = gift.items.sumByDouble { it.realPrice }
		//val totalLabel = Label("Total: credits $totalPrice, money $totalRealPrice")

		val viewButton = JFXButton("Просмотреть").apply {
			setOnAction {
				Platform.runLater {
					val browser = BrowserWindow(gift.url, profile.adminPanel!!.name)
					browser.start(Stage())
				}
			}
		}

		//val spacePne = Pane().also { HBox.setHgrow(it, Priority.ALWAYS) }

		//val bottomBox = HBox(viewButton, spacePne, totalLabel)

		return VBox(headerBox, itemsBox, viewButton).apply {
			spacing = 10.0
			padding = Insets(10.0)
			style = "-fx-background-color: white"
		}
	}

	@Suppress("UNCHECKED_CAST")
	private fun createGiftItemTable(
			items: List<GiftItem>): JFXTreeTableView<RecursiveGiftItem> {
		val nameColumn = JFXTreeTableColumn<RecursiveGiftItem, String>("Name")
		val amountColumn = JFXTreeTableColumn<RecursiveGiftItem, Int>("Amount")
		val creditsColumn = JFXTreeTableColumn<RecursiveGiftItem, Int>("Credits")
		val realPriceColumn = JFXTreeTableColumn<RecursiveGiftItem, Double>("Money")

		setTableCellValueFactory(nameColumn) { it.name }
		setTableCellValueFactory(amountColumn) { it.amount as ObservableValue<Int> }
		setTableCellValueFactory(creditsColumn) { it.credits as ObservableValue<Int> }
		setTableCellValueFactory(realPriceColumn) { it.money as ObservableValue<Double> }

		val recursiveItems = items.map(::RecursiveGiftItem)
		val fxItems = FXCollections.observableArrayList(recursiveItems)

		val totalItem = RecursiveGiftItem(
				GiftItem("Total", 0, items.sumBy { it.price },
						items.sumByDouble { it.realPrice }))
		fxItems.add(totalItem)

		// build tree
		val root = RecursiveTreeItem<RecursiveGiftItem>(fxItems, { it.children })

		val treeView = JFXTreeTableView<RecursiveGiftItem>(root)
		treeView.isShowRoot = false
		treeView.isEditable = false
		treeView.columns.setAll(nameColumn, amountColumn, creditsColumn, realPriceColumn)
		treeView.prefWidth = 120.0

		return treeView
	}

	private fun createButtons(profile: Profile): Array<JFXButton> {
		val okButton = JFXButton("ОК")
		okButton.setOnAction { dialog.close() }

		val detailButton = JFXButton("Подробная информация")
		detailButton.setOnAction {
			Platform.runLater {
				val url = "$EDIT_FEMALE${profile.id}"
				val browser = BrowserWindow(url, profile.adminPanel!!.name)
				browser.start(Stage())
			}
		}

		val addToCalendarButton = JFXButton("Добавить в календарь")
		addToCalendarButton.setOnAction {
			runLater {
				addEventToCalendar()
				val bar = JFXSnackbar(dialogContainer)
				bar.enqueue(JFXSnackbar.SnackbarEvent("Событие добавлено в календарь"))
			}
		}

		/*if (profile.gifts.isNotEmpty()) {
			val giftButton = JFXButton("Посмотреть подарок")
			giftButton.setOnAction {
				profile.gifts.forEach { gift ->
					Platform.runLater {
						val browser = BrowserWindow(gift.url, profile.adminPanel!!.name)
						browser.start(Stage())
					}
				}
			}

			return arrayOf(okButton, detailButton, addToCalendarButton, giftButton)
		}*/

		return arrayOf(okButton, detailButton, addToCalendarButton)
	}

	internal inner class SelectableView(vararg info: String) {
		val box: VBox = VBox()
		val content: Array<SelectableText>

		init {
			val selectableTextArray = info
					.map { SelectableText("$it\n") }
					.toTypedArray()
			content = selectableTextArray
			box.children.addAll(*content)
		}
	}

	internal inner class SelectableText(text: String) : TextField(text) {
		init {
			isEditable = false
			prefColumnCount = 20
			style = "-fx-background-color: transparent; " +
					"-fx-background-insets: 0px;"
		}
	}

	private fun addEventToCalendar() {
		AuthService().addEvent(profile)
	}

	private fun <T : Any> setTableCellValueFactory(column: JFXTreeTableColumn<RecursiveGiftItem, T>,
												   getValue: (RecursiveGiftItem) -> ObservableValue<T>) {
		column.setCellValueFactory { param ->
			if (column.validateValue(param)) {
				getValue(param.value.value)
			} else column.getComputedValue(param)
		}
	}
}