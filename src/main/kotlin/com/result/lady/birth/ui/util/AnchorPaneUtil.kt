package com.result.lady.birth.ui.util

import javafx.scene.Node
import javafx.scene.layout.AnchorPane


object AnchorPaneUtil {

    fun setPosition(node: Node, left: Double, right: Double) {
        AnchorPane.setLeftAnchor(node, left)
        AnchorPane.setRightAnchor(node, right)
    }

    fun setPosition(node: Node, top: Double, left: Double, right: Double) {
        AnchorPane.setTopAnchor(node, top)
        AnchorPane.setLeftAnchor(node, left)
        AnchorPane.setRightAnchor(node, right)
    }

    fun setPosition(node: Node, top: Double, left: Double, right: Double, bottom: Double) {
        AnchorPane.setTopAnchor(node, top)
        AnchorPane.setLeftAnchor(node, left)
        AnchorPane.setRightAnchor(node, right)
        AnchorPane.setBottomAnchor(node, bottom)
    }
}