package com.result.lady.birth.ui.controller

import javafx.fxml.Initializable
import javafx.scene.Node
import javafx.stage.Stage
import java.net.URL
import java.util.*


open class AbstractController : Initializable {

    override fun initialize(location: URL?, resources: ResourceBundle?) {}

    protected fun close(node: Node) {

        println("AbstractController closed")

        val stage: Stage = node.scene.window as Stage
        stage.close()
    }
}