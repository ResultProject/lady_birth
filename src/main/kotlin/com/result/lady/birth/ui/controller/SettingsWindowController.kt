package com.result.lady.birth.ui.controller

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXListView
import com.result.lady.birth.model.AdminPanel
import com.result.lady.birth.service.AdminPanelStorageService
import com.result.lady.birth.service.AuthService
import javafx.application.Platform
import javafx.collections.FXCollections
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.StackPane
import javafx.stage.Stage
import javafx.util.Callback
import java.io.File
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*


class SettingsWindowController : AbstractController() {

	private var panelListToDelete: MutableList<AdminPanel> = mutableListOf()
	private val panelListToSave: MutableList<AdminPanel> = mutableListOf()

	@FXML
	lateinit var root: StackPane

	@FXML
	lateinit var adminPanelListView: JFXListView<AdminPanel>

	@FXML
	private lateinit var loginBtn: Button

	var data = FXCollections.observableArrayList(
			AdminPanel("name1", "login1", "pass1"),
			AdminPanel("name2", "login2", "pass2"),
			AdminPanel("name3", "login3", "pass3"),
			AdminPanel("name4", "login4", "pass4"),
			AdminPanel("name5", "login5", "pass5"),
			AdminPanel("name6", "login6", "pass6"),
			AdminPanel("name7", "login7", "pass7"),
			AdminPanel("name8", "login8", "pass8"),
			AdminPanel("name9", "login9", "pass9"),
			AdminPanel("name10", "login10", "pass10")
	)

	override fun initialize(location: URL?, resources: ResourceBundle?) {
		val restoredPanels = AdminPanelStorageService.adminPanels

		adminPanelListView.items = /*data*/  FXCollections.observableArrayList(restoredPanels)

		adminPanelListView.cellFactory = Callback<ListView<AdminPanel>, ListCell<AdminPanel>> { AdminPanelCell() }

		val sep = File.separator
		val home = System.getProperty("user.home")
		val credPath = Paths.get("$home$sep.vb${sep}google-calendar-credentials${sep}StoredCredential")

		val isAuthorized = Files.exists(credPath)

		if (isAuthorized) {
			loginBtn.isDisable = true
			loginBtn.text = "You are inside"
		}


		loginBtn.setOnAction {
			AuthService().auth()
			loginBtn.isDisable = true
			loginBtn.text = "You are inside"
		}

		adminPanelListView.prefWidth = 200.0
		adminPanelListView.prefHeight = 200.0
	}

	@FXML
	fun addAdminPanel() {
		val stage = Stage()
		val loader = FXMLLoader(javaClass.getResource("/fxml/addPanelDialogView.fxml"))
		val root = loader.load<Parent>()

		val addPanelController = loader.getController<AddPanelDialogController>()
		addPanelController.adminPanelListView = adminPanelListView
		addPanelController.panelListToSave = panelListToSave

		stage.initOwner(adminPanelListView.scene.window)
		stage.scene = Scene(root, 400.0, 300.0)

		stage.show()
	}

	@FXML
	fun save() {
		if (panelListToDelete.isNotEmpty()) {
			AdminPanelStorageService.delete(panelListToDelete)
		}

		if (panelListToSave.isNotEmpty()) {
			AdminPanelStorageService.save(panelListToSave)
		}

		// TODO: if first window is ProfilesListWindow.class - skip
		if (AdminPanelStorageService.adminPanels.isNotEmpty()) {
			Platform.runLater {
				//                WindowFactory.showProfilesListWindow(Stage(), AdminPanelStorageService.adminPanels)

				val stage = Stage()
				val loader = FXMLLoader(javaClass.getResource("/fxml/profilesListView.fxml"))
				val root = loader.load<Parent>()
				stage.scene = Scene(root, 400.0, 300.0)

				stage.show()

				val controller = loader.getController<ProfilesListWindowController>()
				controller.loadData()
			}
			close(adminPanelListView)
		} else {
			// TODO: print error to label
		}
	}

	private fun deletePanel(panel: AdminPanel) {
		panelListToDelete.add(panel)
		adminPanelListView.items.remove(panel)
	}

	inner class AdminPanelCell : ListCell<AdminPanel>() {
		override fun updateItem(item: AdminPanel?, empty: Boolean) {
			super.updateItem(item, empty)

			if (item != null) {
				val box = AnchorPane()

				val deleteButton = JFXButton("Удалить")
				deleteButton.style = "-fx-background-color: #D50000; -fx-text-fill: WHITE"
				val name = Label(item.name)
				// name.setOnMouseClicked { dialog() }

				AnchorPane.setLeftAnchor(name, 20.0)
				AnchorPane.setRightAnchor(deleteButton, 20.0)

				box.resize(box.width, 80.0)

				box.children.addAll(name, deleteButton)

				deleteButton.onAction = EventHandler<ActionEvent> { deletePanel(item) }

				graphic = box
			} else {
				graphic = null
			}
		}
	}
}