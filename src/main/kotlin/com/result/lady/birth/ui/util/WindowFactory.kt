package com.result.lady.birth.ui.util

import com.jfoenix.controls.JFXListView
import com.result.lady.birth.model.AdminPanel
import com.result.lady.birth.ui.AddPanelDialogWindow
import com.result.lady.birth.ui.ProfilesListWindow
import com.result.lady.birth.ui.SettingsWindow
import com.result.lady.birth.ui.controller.AddPanelDialogController
import com.result.lady.birth.ui.controller.ProfilesListWindowController
import javafx.application.Platform
import javafx.stage.Stage


object WindowFactory {

	fun showAddPanelDialogWindow(adminPanelListView: JFXListView<AdminPanel>,
								 panelListToSave: MutableList<AdminPanel>) {
		Platform.runLater {
			val addPanelDialogWindow = AddPanelDialogWindow()
			addPanelDialogWindow.setProScene(Stage())

			val addPanelController = addPanelDialogWindow.getController() as AddPanelDialogController
			addPanelController.adminPanelListView = adminPanelListView
			addPanelController.panelListToSave = panelListToSave
		}
	}

	fun showProfilesListWindow(stage: Stage, adminPanels: List<AdminPanel>) {
		val profilesListWindow = ProfilesListWindow()
		profilesListWindow.setProScene(stage)

		val profileListController = profilesListWindow.getController() as ProfilesListWindowController
		profileListController.loadData()
	}

	fun showSettingsWindow(stage: Stage) {
		Platform.runLater {
			val settingsWindow = SettingsWindow()
			settingsWindow.setProScene(stage)
		}
	}

	fun showBrowserWindow(url: String) {
		/*println("showBrowserWindow")
	   // Platform.runLater {
			println("START")
			val browserWindow = BrowserView()
			browserWindow.start(Stage())
			println("CONTROLLER")
			val controller = browserWindow.getController() as BrowserWindowController
			println("LOAD")
			controller.loadPage(url)
			println("DONE")
		//}*/
	}
}