package com.result.lady.birth.ui

import com.result.lady.birth.ui.controller.AbstractController
import com.result.lady.birth.ui.util.WindowType
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.layout.Pane
import javafx.stage.Stage


abstract class Window {
	lateinit var stage: Stage

	private var fxmlLoader: FXMLLoader? = null

	fun setProScene(primaryStage: Stage?) {
		stage = primaryStage!!
		primaryStage.title = title()

		val loader = FXMLLoader()
		loader.location = Window::class.java
				.classLoader
				.getResource(fxmlResourcePath())

		fxmlLoader = loader

		// val pane: AnchorPane = fxmlLoader!!.load()
		val pane: Pane = fxmlLoader!!.load()

		primaryStage.isResizable = isResizable()

		type().setInitOptions(primaryStage)

		primaryStage.scene = createScene(pane)

		println("WINDOW!!!")
	}

	fun getController(): AbstractController = fxmlLoader!!.getController()


	protected abstract fun title(): String

	protected abstract fun fxmlResourcePath(): String

	protected abstract fun isResizable(): Boolean

	protected abstract fun type(): WindowType

	protected fun createScene(pane: Pane): Scene {
		return Scene(pane)
	}
}