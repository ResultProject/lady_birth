package com.result.lady.birth.ui

import com.result.lady.birth.ui.util.WindowType


class AddPanelDialogWindow : Window() {

    override fun title(): String = "Add Victoria Admin Panel"

    override fun fxmlResourcePath(): String = "fxml/addPanelDialogView.fxml"

    override fun type(): WindowType = WindowType.DIALOG

    override fun isResizable(): Boolean = false
}