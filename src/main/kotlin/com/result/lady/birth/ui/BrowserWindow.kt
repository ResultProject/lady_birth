package com.result.lady.birth.ui

import com.result.lady.birth.service.ClientManager
import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.layout.AnchorPane
import javafx.stage.Stage

class BrowserWindow(val url: String, val adminPanelName: String) : Application() {

    override fun start(primaryStage: Stage?) {
        primaryStage!!.title = "Дополнительная информация"

        val cookieManager = ClientManager.getClient(adminPanelName)
                .getCookieManager()
        val browser = BrowserView(url, cookieManager)

        val pane = AnchorPane()
        pane.children.addAll(browser.getElements())

        primaryStage.scene = Scene(pane)
        primaryStage.show()
    }
}