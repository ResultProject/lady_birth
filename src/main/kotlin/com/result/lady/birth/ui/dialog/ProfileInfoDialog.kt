package com.result.lady.birth.ui.dialog

import com.jfoenix.controls.JFXButton
import com.jfoenix.controls.JFXDialog
import com.jfoenix.controls.JFXDialogLayout
import com.result.lady.birth.model.Profile
import com.result.lady.birth.service.ClientManager
import com.result.lady.birth.ui.BrowserWindow
import javafx.application.Platform
import javafx.scene.control.TextField
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import javafx.stage.Stage

class ProfileInfoDialog(profile: Profile, rootPane: StackPane) {

	private val EDIT_FEMALE = "${ClientManager.BASE_URL}/manager/edit-female/"

	val dialog: JFXDialog

	init {
		dialog = create(profile, rootPane)
	}

	fun show() {
		dialog.show()
	}

	private fun create(profile: Profile, rootPane: StackPane): JFXDialog {
		val content = JFXDialogLayout()
		content.setHeading(SelectableText(profile.name))

		val bodyText = createSelectableView(profile)
		content.setBody(bodyText.box)

		val dialog = JFXDialog(rootPane, content, JFXDialog.DialogTransition.CENTER)

		val buttons = createButtons(profile)
		content.setActions(*buttons)

		return dialog
	}

	private fun createSelectableView(profile: Profile): SelectableView {
		return SelectableView("ID = ${profile.id}",
				"Дата рождения = ${profile.birthday}",
				"Переводчик = ${profile.translatorEmail}",
				"Админка = ${profile.adminPanel!!.name}")
	}

	private fun createButtons(profile: Profile): Array<JFXButton> {
		val okButton = JFXButton("ОК")
		okButton.setOnAction { dialog.close() }

		val detailButton = JFXButton("Подробная информация")
		detailButton.setOnAction {
			Platform.runLater {
				val url = "$EDIT_FEMALE${profile.id}"
				val browser = BrowserWindow(url, profile.adminPanel!!.name)
				browser.start(Stage())
			}
		}

		val addToCalendarButton = JFXButton("Добавить в календарь")
		addToCalendarButton.setOnAction {
			// TODO: add to calendar
		}

		if (profile.gifts.isNotEmpty()) {
			val giftButton = JFXButton("Посмотреть подарок")
			giftButton.setOnAction {
				profile.gifts.forEach { gift ->
					Platform.runLater {
						val browser = BrowserWindow(gift.url, profile.adminPanel!!.name)
						browser.start(Stage())
					}
				}
			}

			println("BUILT BUTTON FOR GIFTS")
			return arrayOf(okButton, detailButton, addToCalendarButton, giftButton)
		}

		return arrayOf(okButton, detailButton, addToCalendarButton)
	}

	internal inner class SelectableView(vararg info: String) {
		val box: VBox = VBox()
		val content: Array<SelectableText>

		init {
			val selectableTextArray = info
					.map { SelectableText("$it\n") }
					.toTypedArray()
			content = selectableTextArray
			box.children.addAll(*content)
		}
	}

	internal inner class SelectableText(text: String) : TextField(text) {
		init {
			isEditable = false
			prefColumnCount = 20
			style = "-fx-background-color: transparent; " +
					"-fx-background-insets: 0px;"
		}
	}
}