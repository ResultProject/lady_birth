package com.result.lady.birth.ui.controller

import com.jfoenix.controls.JFXListView
import com.jfoenix.controls.JFXProgressBar
import com.result.lady.birth.model.DeliveryStatus
import com.result.lady.birth.model.Profile
import com.result.lady.birth.service.AdminPanelStorageService
import com.result.lady.birth.service.ResponseListener
import com.result.lady.birth.service.ServiceRunner
import com.result.lady.birth.ui.ProfileInfoDialog
import com.result.lady.birth.ui.util.AnchorPaneUtil
import com.result.lady.birth.util.DateUtil
import de.jensd.fx.glyphs.GlyphsDude
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import javafx.application.Platform
import javafx.fxml.FXML
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.scene.layout.AnchorPane
import javafx.scene.layout.HBox
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import javafx.scene.text.Text
import javafx.stage.Modality
import javafx.stage.Stage
import javafx.util.Callback
import reactor.core.publisher.Flux
import java.net.URL
import java.util.*


class ProfilesListWindowController : AbstractController(), ResponseListener<Flux<Profile>> {

	private val profilesListView: JFXListView<Profile> = JFXListView()

	private var isLoaded = false

	private var alldata: MutableList<Profile> = mutableListOf()

	private var panelCounter = 0

	@FXML
	lateinit var loading: JFXProgressBar

	@FXML
	lateinit var rootProfilesListPane: StackPane

	@FXML
	lateinit var controlPane: AnchorPane

	@FXML
	lateinit var itemsContainer: VBox

	@FXML
	private lateinit var settingsBtn: Button

	override fun initialize(location: URL?, resources: ResourceBundle?) {
		super.initialize(location, resources)

		settingsBtn.isVisible = false

		/*AnchorPane.setTopAnchor(itemsContainer, 10.0)
		AnchorPane.setRightAnchor(itemsContainer, 0.0)
		AnchorPane.setLeftAnchor(itemsContainer, 0.0)*/

		profilesListView.cellFactory = Callback<ListView<Profile>, ListCell<Profile>> { ProfileCell() }

		profilesListView.setOnMouseClicked { showProfileInfoDialog(profilesListView.selectionModel.selectedItem) }

		/*profilesListView.prefWidth = 200.0
		profilesListView.prefHeight = 550.0*/

		/*AnchorPane.setTopAnchor(profilesListView, 50.0)
		AnchorPane.setRightAnchor(profilesListView, 0.0)
		AnchorPane.setLeftAnchor(profilesListView, 0.0)
		AnchorPane.setBottomAnchor(profilesListView, 0.0)

		AnchorPane.setTopAnchor(rootProfilesListPane, 0.0)
		AnchorPane.setRightAnchor(rootProfilesListPane, 0.0)
		AnchorPane.setLeftAnchor(rootProfilesListPane, 0.0)
		AnchorPane.setBottomAnchor(rootProfilesListPane, 0.0)

		SizeBinder.add(controlPane)
		SizeBinder.add(rootProfilesListPane)*/


	}

	@FXML
	fun openSettings() {
		val stage = Stage()
		val thisWindow = rootProfilesListPane.scene.window
		stage.initModality(Modality.APPLICATION_MODAL)

		stage.initOwner(thisWindow)
		val loader = FXMLLoader(javaClass.getResource("/fxml/settingsView.fxml"))
		val root = loader.load<Parent>()
		stage.scene = Scene(root)
		stage.showAndWait()
	}

	fun loadData() {
		val panels = AdminPanelStorageService.adminPanels
		ServiceRunner.run(panels, this)
	}

	override fun success(data: Flux<Profile>) {
		alldata.addAll(data.toIterable())
		panelCounter++

		if (AdminPanelStorageService.adminPanels.size == panelCounter) {

			settingsBtn.isVisible = true

			alldata.sortBy { DateUtil.convertToBirthdayInThisYear(it.birthday) }

			profilesListView.items.addAll(alldata)

			Platform.runLater {
				if (!isLoaded) {

					isLoaded = true

					AnchorPaneUtil.setPosition(profilesListView, 50.0, 0.0, 0.0, 0.0)

					itemsContainer.children.remove(loading)
					controlPane.children.add(profilesListView)
				}
			}
		}
	}

	private fun showProfileInfoDialog(profile: Profile) {
		val dialog = ProfileInfoDialog(profile, rootProfilesListPane)
		dialog.show()
	}

	inner class ProfileCell : ListCell<Profile>() {

		override fun updateItem(item: Profile?, empty: Boolean) {
			super.updateItem(item, empty)

			if (item != null) {
				val thisYearBirthDay = DateUtil.convertToBirthdayInThisYear(item.birthday)
				val birthday = DateUtil.getFormatted(thisYearBirthDay)
				val info = Label("${item.name} - $birthday")
				val daysToEvent = DateUtil.daysToEvent(thisYearBirthDay)

				if (item.gifts.isNotEmpty()) {
					info.graphic = HBox().apply {
						spacing = 5.0

						val icons = item.gifts.map {
							createIconByStatus(it.status)
						}
						children.addAll(icons)
					}
				}

				if (!item.isCanReceive) {
					info.graphic = GlyphsDude.createIcon(FontAwesomeIcon.BAN)
				}

				when {
					daysToEvent < 0 -> info.style = "-fx-text-fill: #757575;"
					daysToEvent in 0..3 -> info.style = "-fx-text-fill: #D50000;"
					daysToEvent in 4..7 -> info.style = "-fx-text-fill: #2e7d32;"
				}

				graphic = info
			} else {
				graphic = null
			}
		}
	}

	private fun createIconByStatus(status: DeliveryStatus): Text {
		val icon = when (status) {
			DeliveryStatus.PENDING -> FontAwesomeIcon.TRUCK
			DeliveryStatus.PROCESSING -> FontAwesomeIcon.TRUCK
			DeliveryStatus.FAILED -> FontAwesomeIcon.UNDO
			DeliveryStatus.FAILED_BY_BLOCK -> FontAwesomeIcon.UNDO
			else -> FontAwesomeIcon.GIFT
		}
		return GlyphsDude.createIcon(icon)
	}

/*    private fun addEventToCalendar()
    {
        val selProfile = profilesListView.selectionModel.selectedItem
        AuthService().addEvent(selProfile)
    }*/
}