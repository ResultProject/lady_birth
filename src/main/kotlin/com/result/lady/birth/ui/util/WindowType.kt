package com.result.lady.birth.ui.util

import javafx.stage.Modality
import javafx.stage.Stage


enum class WindowType {

    WINDOW {
        override fun setInitOptions(stage: Stage?) {

        }
    },
    DIALOG {
        override fun setInitOptions(stage: Stage?)
        {
            stage?.initModality(Modality.WINDOW_MODAL)
        }
    };

    abstract fun setInitOptions(stage: Stage?)
}