package com.result.lady.birth.ui

import com.result.lady.birth.ui.util.WindowType


class SettingsWindow : Window() {

	override fun title(): String = "Настройки"

	override fun fxmlResourcePath(): String = "fxml/settingsView.fxml"

	override fun isResizable(): Boolean = false

	override fun type(): WindowType = WindowType.WINDOW

	companion object {
		@JvmStatic
		fun main(args: Array<String>) {
//            launch(SettingsWindow::class.java)
		}
	}
}