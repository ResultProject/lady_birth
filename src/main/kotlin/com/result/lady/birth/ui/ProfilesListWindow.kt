package com.result.lady.birth.ui

import com.result.lady.birth.ui.util.WindowType


class ProfilesListWindow : Window() {

    override fun title(): String = "VictoriaBirthday"

    override fun fxmlResourcePath(): String = "fxml/profilesListView.fxml"

    override fun isResizable(): Boolean = false

    override fun type(): WindowType = WindowType.WINDOW

/*    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(ProfilesListWindow::class.java)
        }
    }

    override fun stop() {
        super.stop()
        OwnerStage.owner?.close()
        Platform.exit()
    }*/
}