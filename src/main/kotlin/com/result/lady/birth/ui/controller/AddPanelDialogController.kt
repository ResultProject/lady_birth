package com.result.lady.birth.ui.controller

import com.jfoenix.controls.JFXListView
import com.jfoenix.controls.JFXPasswordField
import com.jfoenix.controls.JFXTextField
import com.result.lady.birth.model.AdminPanel
import javafx.application.Platform
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.stage.Stage
import java.net.URL
import java.util.*


class AddPanelDialogController : AbstractController() {

    lateinit var adminPanelListView: JFXListView<AdminPanel>
    lateinit var panelListToSave: MutableList<AdminPanel>

    @FXML
    lateinit var panelNameTextField: JFXTextField

    @FXML
    lateinit var panelLoginTextField: JFXTextField

    @FXML
    lateinit var panelPasswordField: JFXPasswordField

    @FXML
    lateinit var messageLabel: Label

    override fun initialize(location: URL?, resources: ResourceBundle?) {
    }

    @FXML
    fun cancel() {
        val stage = panelNameTextField.scene.window as Stage
        stage.close()
    }

    @FXML
    fun save() {
        val name = panelNameTextField.text.trim()
        val login = panelLoginTextField.text.trim()
        val pass = panelPasswordField.text.trim()

        if (name.isNullOrEmpty() || login.isNullOrEmpty() || pass.isNullOrEmpty()) {
            setErrorMessage()
        } else {
            val panel = AdminPanel(name, login, pass)
            savePanel(panel)
        }
    }

    private fun setErrorMessage() {
        Platform.runLater {
            messageLabel.text = "Пожалуйста заполните все поля"
            messageLabel.style = "-fx-text-fill: #d50000"
        }
    }

    private fun savePanel(panel: AdminPanel) {
        if (adminPanelListView != null && panelListToSave != null) {
            panelListToSave.add(panel)
            adminPanelListView.items.add(panel)
        }
        close(panelLoginTextField)
    }

    @FXML
    private fun keyListener(event: KeyEvent)
    {
        if (event.code == KeyCode.ENTER)
        {
            save()

            event.consume()
        }
    }
}