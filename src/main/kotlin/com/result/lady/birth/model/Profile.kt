package com.result.lady.birth.model

import java.time.LocalDate


data class Profile(var id: String, var name: String, var birthday: LocalDate, var translatorEmail: String, val isCanReceive: Boolean,
				   var gifts: MutableList<Gift> = mutableListOf(), var adminPanel: AdminPanel? = null)