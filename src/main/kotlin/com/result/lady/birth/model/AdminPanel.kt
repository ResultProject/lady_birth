package com.result.lady.birth.model

import java.io.Serializable


data class AdminPanel(var name: String, var login: String, var password: String) : Serializable