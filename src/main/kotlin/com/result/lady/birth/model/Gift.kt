package com.result.lady.birth.model

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleStringProperty


data class Gift(val sender: String, val items: List<GiftItem>, val url: String, val status: DeliveryStatus)

data class GiftItem(val name: String, val amount: Int, val price: Int, val realPrice: Double)

class RecursiveGiftItem(item: GiftItem) : RecursiveTreeObject<RecursiveGiftItem>() {
	val name = SimpleStringProperty(item.name)
	val amount = SimpleIntegerProperty(item.amount)
	val credits = SimpleIntegerProperty(item.price)
	val money = SimpleDoubleProperty(item.realPrice)
}

enum class DeliveryStatus {
	PENDING,
	PROCESSING,
	DELIVERED,
	APPROVED,
	FAILED,
	FAILED_BY_BLOCK,
	RECEIVED
}