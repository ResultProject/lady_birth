package com.result.lady.birth.http.controller

import com.result.lady.birth.Config
import com.result.lady.birth.http.Client
import com.result.lady.birth.model.AdminPanel
import com.result.lady.birth.model.Profile
import com.result.lady.birth.util.DateUtil
import okhttp3.Call
import java.time.LocalDate


class VictoriaHttpController(val client: Client) {

	fun auth(adminPanel: AdminPanel): Call {
		return client.post("/operator/login",
				mapOf("email" to adminPanel.login, "password" to adminPanel.password))
	}

	fun getAllProfilesPage(): Call {
		return client
				.post("/account/index",
						mapOf("operators" to "-1"))
	}

	fun getProfilePage(link: String): Call {
		return client
				.get(link)
	}

	fun getApprovedGiftsPage(profile: Profile): Call {
		return getGiftsPage(profile, "approved")
	}

	fun getReceivedGiftsPage(profile: Profile): Call {
		return getGiftsPage(profile, "received")
	}

	fun getAllGiftPage(profile: Profile): Call {
		return getGiftsPage(profile, "0")
	}

	fun getGiftsPage(profile: Profile, status: String): Call {
		val bithDay = DateUtil.convertToBirthdayInThisYear(profile.birthday)

		val beforeDay = bithDay.minusDays(Config.getBefore())
		var afterDay = bithDay.plusDays(Config.getAfter())

		val today = LocalDate.now()

		if (afterDay.toEpochDay() > today.toEpochDay()) {
			afterDay = today
		}

		return client.post("/manager/real-gifts-list",
				mapOf("id_female" to profile.id,
						"status" to status,
						"from" to DateUtil.victoriaFormatter.format(beforeDay),
						"to" to DateUtil.victoriaFormatter.format(afterDay)
				)
		)
	}

	fun withCustom(url: String): Call {
		return client.get("/$url")
	}
}