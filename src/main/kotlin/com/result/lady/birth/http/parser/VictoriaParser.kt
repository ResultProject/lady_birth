package com.result.lady.birth.http.parser

import com.result.lady.birth.model.Profile
import org.apache.logging.log4j.LogManager
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import javax.script.Invocable
import javax.script.ScriptEngineManager


class VictoriaParser {

	private val log = LogManager.getLogger()

	private val dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

	private val factory by lazy { ScriptEngineManager() }
	private val engine by lazy { factory.getEngineByName("JavaScript") }

	private var isEngineEval = false

	fun parseEditLinksOnProfilesPage(page: Mono<String>): Flux<String> {
		log.debug("parseEditLinksOnProfilesPage")
		return page.flatMapIterable { html: String ->
			Jsoup.parse(html)
					.getElementsByAttributeValueContaining("href", "/manager/edit-female")
					.toList()
					.map { htmlElement: Element -> htmlElement.attr("href") }
		}
	}

	fun parseProfileFromPage(page: Mono<String?>): Mono<Profile> {
		log.debug("parseProfileFromPage")
		return page.map {
			val document = Jsoup.parse(it)

			val id = parseProfileId(document)

			val name = parseProfileName(document)

			val birthday = parseProfileBirthday(document)

			val translatorEmail = parseTranslatorEmail(document)

			val isCanReceive = parseCanReceiveGifts(document).let {
				if (it.isEmpty())
					return@let false

				if (it == "true")
					return@let true

				return@let it.toBoolean()
			}

			return@map Profile(id, name, birthday, translatorEmail, isCanReceive)
		}
	}

	private fun parseProfileId(document: Document): String {
		log.debug("parseProfileId")
		return document
				.getElementsByAttribute("data-id-user")
				.first()
				.text()
	}

	private fun parseProfileName(document: Document): String {
		log.debug("parseProfileName")
		return document
				.getElementById("name")
				.attr("value")
	}

	private fun parseProfileBirthday(document: Document): LocalDate {
		log.debug("parseProfileBirthday")
		val birthday = document
				.getElementById("date_birth")
				.attr("value")

		return LocalDate.parse(birthday, dateFormatter)
	}

	private fun parseTranslatorEmail(document: Document): String {
		log.debug("parseTranslatorEmail")
		val select = document
				.getElementById("operators")
		val selectItem = select.getElementsByAttributeValue("selected", "selected").first()

		return if (selectItem.html().contains("data-cfemail")) {
			val cfEmail = selectItem.getElementsByClass("__cf_email__")
					.first()
					.attr("data-cfemail")
			decodeTranslatorEmail(cfEmail) ?: ""
		} else selectItem.text()
	}

	private fun parseCanReceiveGifts(document: Document): String {
		log.debug("parseCanReceiveGifts")
		return document
				.getElementById("can_receive_gift")
				.attr("checked")
	}

	private fun decodeTranslatorEmail(dataCfemail: String, magicNum: Int = 0): String? {
		try {
			if (!isEngineEval) {
				engine.eval("function errorCatch(e) {\n" +
						"    try {\n" +
						"        if (\"undefined\" == typeof console) return;\n" +
						"    } catch (e) {\n" +
						"    }\n" +
						"}\n" +
						"\n" +
						"function integerDecode(element, someNum) {\n" +
						"    var r = element.substr(someNum, 2);\n" +
						"    return parseInt(r, 16)\n" +
						"}\n" +
						"\n" +
						"function transform(cfemailAttribute, num) {\n" +
						"    for (var result = \"\", byteDecodeNum = integerDecode(cfemailAttribute, num), i = num + 2; i < cfemailAttribute.length; i += 2) {\n" +
						"        var f = integerDecode(cfemailAttribute, i) ^ byteDecodeNum;\n" +
						"        result += String.fromCharCode(f)\n" +
						"    }\n" +
						"    try {\n" +
						"        result = decodeURIComponent(escape(result))\n" +
						"    } catch (l) {\n" +
						"        errorCatch(l)\n" +
						"    }\n" +
						"    //return createDiv(result)\n" +
						"    return result\n" +
						"}\n" +
						"\n" +
						"var emailProtectionLink = \"/cdn-cgi/l/email-protection#\",\n" +
						"    ch_email_under = \".__cf_email__\",\n" +
						"    cfemailName = \"data-cfemail\";")

				isEngineEval = true
			}

			val inv = engine as Invocable

			return inv.invokeFunction("transform", dataCfemail, magicNum) as String
		} catch (e: Exception) {
			e.printStackTrace()
			throw e
		}
	}
}