package com.result.lady.birth.http

import okhttp3.*
import org.apache.logging.log4j.LogManager
import java.io.File
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit


class Client(val baseUrl: String) {
	private var cookieManager: CookieManager? = null

	private val log = LogManager.getLogger()

	protected val client: OkHttpClient

	init {
		val executorService = Executors.newWorkStealingPool(Runtime.getRuntime().availableProcessors() * 2)
		val myDispatcher = Dispatcher(executorService)

		client = OkHttpClient.Builder()
				.cookieJar(JavaNetCookieJar(getCookieManager()))
				.connectTimeout(2, TimeUnit.MINUTES)
				.readTimeout(2, TimeUnit.MINUTES)
				.writeTimeout(2, TimeUnit.MINUTES)
				.addInterceptor(LoggingInterceptor())
				.cache(getCache("victoria"))
				.dispatcher(myDispatcher)
				.build()

		//executorService.shutdown()
	}

	fun get(url: String): Call {
		val request = Request.Builder()
				.url("$baseUrl$url")
				.get()
				.build()

		return client.newCall(request)
	}

	fun get(url: String, params: Map<String, String>): Call {
		val builtUrl = mapToUrl("$baseUrl$url", params)

		val request = Request.Builder()
				.url(builtUrl)
				.get()
				.build()

		return client.newCall(request)
	}

	fun post(url: String, params: Map<String, String>): Call {
		val builder = FormBody.Builder()
		params.forEach { (key, value) -> builder.add(key, value) }
		val body: RequestBody = builder.build()

		val request = Request.Builder()
				.url("$baseUrl$url")
				.post(body)
				.build()

		return client.newCall(request)
	}

	fun getCookieManager(): CookieManager {
		if (cookieManager == null) {
			cookieManager = CookieManager()
			cookieManager?.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
		}
		return cookieManager!!
	}

	fun getCookieJar() = client.cookieJar()!!

	fun getBodyAndClose(response: Response): String? {
		val body = response.body()?.string()
		response.close()
		return body
	}

	fun mapToUrl(hostUrl: String, params: Map<String, String>): HttpUrl? {
		val urlBuilder = HttpUrl.Builder()
				.host(hostUrl)

		params.forEach { (key, value) ->
			urlBuilder.addQueryParameter(key, value)
		}

		return urlBuilder.build()
	}

	fun callShutdown() {
		client.dispatcher().executorService().shutdown()
	}

	private fun getCache(child: String): Cache {
		val cacheDir = File(System.getProperty("java.io.tmpdir"), child)
		return Cache(cacheDir, 1024)
	}

	inner class LoggingInterceptor : Interceptor {
		override fun intercept(chain: Interceptor.Chain): Response {
			val request = chain.request()

			log.info("method: ${request.method()}, url: ${request.url()}")
			val response = chain.proceed(request)
			val body = response.body()

			return if (body == null) {
				response.newBuilder().build()
			} else {
				response.newBuilder().body(
						ResponseBody.create(response.body()!!.contentType(), body.contentLength(),
								body.source())).build()
			}
		}
	}
}