package com.result.lady.birth.util

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*


object DateUtil {

    val formatter = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale("ru"))

    val victoriaFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

    fun convertToBirthdayInThisYear(birthday: LocalDate): LocalDate {
        val today = LocalDate.now()
        return LocalDate.of(today.year,
                birthday.month.value,
                birthday.dayOfMonth)
    }

    fun getFormatted(date: LocalDate): String = date.format(formatter)

    fun daysToEvent(date: LocalDate): Long {
        val today = LocalDate.now()
        val daysUntilEvent = ChronoUnit.DAYS.between(today, date)
        return daysUntilEvent
    }
}