package com.result.lady.birth.service

import com.result.lady.birth.model.AdminPanel
import com.result.lady.birth.model.Profile
import com.result.lady.birth.util.exception.BadCredentialException
import javafx.scene.control.Alert
import org.apache.logging.log4j.LogManager
import reactor.core.publisher.Flux
import java.time.LocalDate


object ServiceRunner {

	private val log = LogManager.getLogger()

	@Throws(Exception::class)
	fun run(panels: List<AdminPanel>, listener: ResponseListener<Flux<Profile>>) {
		log.info("run")
		if (panels.isNotEmpty()) {

			panels.forEach {
				runService(it, listener)
			}

		} else {
			val response = Profile("-1", "Админ панели не зарегистрированны в программе", LocalDate.now(), "", true)
			listener.success(Flux.just(response))
		}
	}

	private fun runService(panel: AdminPanel, listener: ResponseListener<Flux<Profile>>) {
		log.info("runService panel: ${panel.name}")
		try {
			val service = ServiceFactory.createService(panel)

			service.auth()
			service.getProfiles(listener)
		} catch (e: BadCredentialException) {
			log.error("Incorrect login or pass", e)
			showBadCredentialAlert(panel)
		} catch (e: Exception) {
			log.error("Ooops", e)
			throw e
		}
	}

	private fun showBadCredentialAlert(panel: AdminPanel) {
		log.info("showBadCredentialAlert")
		val head = "Неверные данные"
		val message = "Неверные логин и пароль в ${panel.name} админке." +
				" Пожалуйста перейдите в Настройке и измените данные для вхожа"

		val alert = Alert(Alert.AlertType.ERROR)
		alert.title = "Ошибка"
		alert.headerText = head
		alert.contentText = message

		alert.showAndWait()
	}
}