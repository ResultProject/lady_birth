package com.result.lady.birth.service

import com.result.lady.birth.http.Client
import java.io.StringReader
import java.util.*

object ClientManager {

    private const val domainsFileUrl = "https://raw.githubusercontent.com/MaxNeutrino/domains_raw/master/domains.properties"

    private const val siteDomainKey = "victoria"

    val BASE_URL: String by lazy {
        val domainClient = Client(domainsFileUrl)

        val domains = domainClient.get("").execute().body()?.string() ?: ""

        val properties = Properties()
        properties.load(StringReader(domains))

        return@lazy properties.getProperty(siteDomainKey) ?: ""
    }

    private val clientStore: MutableMap<String, Client> = mutableMapOf()

    fun getClient(panelName: String): Client {
        if (clientStore.contains(panelName))
            return clientStore[panelName]!!
        else
            return saveClient(panelName, createClient())
    }

    private fun saveClient(panelName: String, client: Client): Client {
        clientStore.put(panelName, client)
        return client
    }

    private fun createClient() = Client(BASE_URL)
}