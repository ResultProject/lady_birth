package com.result.lady.birth.service

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.DateTime
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.services.calendar.CalendarScopes
import com.google.api.services.calendar.model.Event
import com.google.api.services.calendar.model.EventDateTime
import com.result.lady.birth.model.Profile
import com.result.lady.birth.util.DateUtil
import org.apache.logging.log4j.LogManager
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.util.*

class AuthService {
	private val log = LogManager.getLogger()

	fun auth() {
		log.info("auth")
		val service = getCalendarService()
	}

	fun addEvent(profile: Profile) {
		log.info("addEvent profileID: ${profile.id}")
		val service = getCalendarService()


		val thisYearBirthdayDate = DateUtil.convertToBirthdayInThisYear(profile.birthday)

		val beganTime = thisYearBirthdayDate.toString() + "T09:00:00+03:00"
		val endTime = thisYearBirthdayDate.toString() + "T18:00:00+03:00"

		val eventName = "День Рождения"
		val eventDescription = "Имя: ${profile.name}, ID: ${profile.id}, " +
				"почтовый ящик переводчика: ${profile.translatorEmail}, " +
				"админ панель: ${profile.adminPanel!!.name}"

		var event = Event()
				.setSummary(eventName)
				.setDescription(eventDescription)


		val startDateTime = DateTime(beganTime)
		val start = EventDateTime()
				.setDateTime(startDateTime)
				.setTimeZone("America/Los_Angeles")
		event.start = start

		val endDateTime = DateTime(endTime)
		val end = EventDateTime()
				.setDateTime(endDateTime)
				.setTimeZone("America/Los_Angeles")
		event.end = end


		val calendarId = "primary"
		event = service.events().insert(calendarId, event).execute()
		System.out.printf("Event created: %s\n", event.htmlLink)
	}

	/** Application name.  */
	private val APPLICATION_NAME = "Google Calendar API Java Quickstart"

	/** PROS */
	private val SEP = File.separator

	/** Directory to store user credentials for this application.  */
	private val DATA_STORE_DIR = java.io.File(
			System.getProperty("user.home"), ".vb${SEP}google-calendar-credentials")

	/** Global instance of the [FileDataStoreFactory].  */
	private var DATA_STORE_FACTORY: FileDataStoreFactory? = null

	/** Global instance of the JSON factory.  */
	private val JSON_FACTORY = JacksonFactory.getDefaultInstance()

	/** Global instance of the HTTP transport.  */
	private var HTTP_TRANSPORT: HttpTransport? = null

	/** Global instance of the scopes required by this quickstart.

	 * If modifying these scopes, delete your previously saved credentials
	 * at ~/.credentials/calendar-java-quickstart
	 */
	private val SCOPES = Arrays.asList(CalendarScopes.CALENDAR)

	/**
	 * Creates an authorized Credential object.
	 * @return an authorized Credential object.
	 * *
	 * @throws IOException
	 */
	@Throws(IOException::class)
	fun authorize(): Credential {
		log.info("authorize")
		// Load client secrets.
		val `in` = AuthService::class.java.getResourceAsStream("/client_secret.json")
		val clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, InputStreamReader(`in`))

		// Build flow and trigger user authorization request.
		val flow = GoogleAuthorizationCodeFlow.Builder(
				HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
				.setDataStoreFactory(DATA_STORE_FACTORY!!)
				.setAccessType("offline")
				.build()

		val credential = AuthorizationCodeInstalledApp(

				flow, LocalServerReceiver()).authorize("user")
		println(
				"Credentials saved to " + DATA_STORE_DIR.absolutePath)

		return credential
	}

	/**
	 * Build and return an authorized Calendar client service.
	 * @return an authorized Calendar client service
	 * *
	 * @throws IOException
	 */
	@Throws(IOException::class)
	fun getCalendarService(): com.google.api.services.calendar.Calendar {
		log.info("getCalendarService")
		val credential = authorize()

		val res = com.google.api.services.calendar.Calendar.Builder(
				HTTP_TRANSPORT!!, JSON_FACTORY, credential)
				.setApplicationName(APPLICATION_NAME)
				.build()

		return res
	}

	init {
		try {
			HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport()
			DATA_STORE_FACTORY = FileDataStoreFactory(DATA_STORE_DIR)
		} catch (t: Throwable) {
			t.printStackTrace()
			System.exit(1)
		}
	}
}


/*        val service = getCalendarService()
        var event = Event()
                .setSummary("Kill people")
                .setDescription("Clean the world")


        val startDateTime = DateTime("2017-07-14T01:00:00+03:00")
        val setProScene = EventDateTime()
                .setDateTime(startDateTime)
                .setTimeZone("America/Los_Angeles")
        event.setProScene = setProScene

        val endDateTime = DateTime("2017-07-14T23:00:00+03:00")
        val end = EventDateTime()
                .setDateTime(endDateTime)
                .setTimeZone("America/Los_Angeles")
        event.end = end


        val calendarId = "primary"
        event = service.events().insert(calendarId, event).execute()
        System.out.printf("Event created: %s\n", event.htmlLink)*/