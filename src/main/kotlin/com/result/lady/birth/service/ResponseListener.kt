package com.result.lady.birth.service


interface ResponseListener<in T> {
    fun success(data: T)
}