package com.result.lady.birth.service

import com.result.lady.birth.http.controller.VictoriaHttpController
import com.result.lady.birth.http.parser.VictoriaParser
import com.result.lady.birth.model.*
import com.result.lady.birth.util.DateUtil
import com.result.lady.birth.util.exception.BadCredentialException
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Response
import org.apache.logging.log4j.LogManager
import org.jsoup.Jsoup
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.io.IOException


class VictoriaService(val adminPanel: AdminPanel,
					  val httpController: VictoriaHttpController) {

	private val log = LogManager.getLogger()

	private val parser = VictoriaParser()

	@Throws(BadCredentialException::class)
	fun auth() {
		log.info("auth")
		val response = httpController
				.auth(adminPanel)
				.execute()

		val json = response?.body()?.string()
		response?.close()
		if (json?.contains("false")!!)
			throw BadCredentialException()
	}

	fun getProfiles(listener: ResponseListener<Flux<Profile>>) {
		log.info("profiles")
		httpController
				.getAllProfilesPage()
				.enqueue(object : Callback {
					override fun onFailure(call: Call?, e: IOException?) {
						log.error("Failure in request time", e)
						call!!.cancel()
					}

					override fun onResponse(call: Call?, response: Response?) {
						log.debug("onResponse $response")
						val html = response?.body()?.string()
						response?.close()

						val profiles: Flux<Profile> = parser
								.parseEditLinksOnProfilesPage(Mono.just(html))
								.flatMap { uri -> parseProfileInfo(uri) }
								.map { profile ->
									val birthdayThisYear = DateUtil.convertToBirthdayInThisYear(profile.birthday)
									val daysUntilBirthday = DateUtil.daysToEvent(birthdayThisYear)

									if (daysUntilBirthday < 0)
										profile.gifts = getGifts(profile)

									profile.adminPanel = adminPanel
									return@map profile
								}

						listener.success(filter(profiles))
						closeAsynRequest()
					}
				})
	}

	fun getGifts(profile: Profile): MutableList<Gift> {
		log.info("parseGifts profileID: ${profile.id}")
		val giftTable = httpController.getAllGiftPage(profile)
				.execute()
				.body()
				?.string()

		val approvedGifts = parseGiftsTable(giftTable ?: "")

		return approvedGifts
	}

	private fun parseGiftsTable(page: String): MutableList<Gift> {
		log.debug("parseGiftsTable")
		try {
			val table = Jsoup.parse(page)
					.getElementsByTag("table")
					.last()
					.getElementsByTag("tbody")
					.first()

			if (table.text().trim().isEmpty())
				return mutableListOf()

			val rows = table.getElementsByTag("tr")
			return rows.map {
				val concreteGiftInfo = it.getElementsByTag("td").let {
					val index = it.size - 2 // last - photo, prelast - status
					it[index]
				}

				val sender = it.getElementsByTag("td")[2].text()

				val giftUrl = concreteGiftInfo.getElementsByTag("a").first().attr("href")
				val giftStatus = concreteGiftInfo.text()

				val status = DeliveryStatus.valueOf(giftStatus.toUpperCase())
				val items = parseGiftItems("/manager/$giftUrl")

				Gift(sender, items, "${ClientManager.BASE_URL}/manager/$giftUrl", status)
			}.toMutableList()
		} catch (e: Exception) {
			log.error("parsing exception", e)
			return mutableListOf()
		}
	}

	private fun parseGiftItems(url: String): List<GiftItem> {
		log.debug("parseGiftItems url: $url")
		val response = httpController.client.get(url).execute()
		val page = response.body()?.string() ?: return emptyList()

		val document = Jsoup.parse(page)
		val table = document.getElementsByClass("items_table").first()
		val tBody = table.getElementsByTag("tbody").first()
		val rows = tBody.getElementsByTag("tr").also {
			// remove Total row
			it.removeAt(it.size - 1)
		}

		return rows.map {
			/**
			 * 0 - item id
			 * 1 - item name
			 * 2 - amount
			 * 3 - agency price
			 */
			val columns = it.getElementsByTag("td")
			GiftItem(
					name = columns[1].text(),
					amount = columns[2].text().toInt(),
					price = columns[3].text().toInt(),
					realPrice = columns[3].text().toDouble() * 0.06
			)
		}
	}

	private fun parseProfileInfo(uri: String): Mono<Profile> {
		log.debug("parseProfileInfo url: $uri")
		val page = Mono.just(httpController
				.getProfilePage(uri)
				.execute())
				.map {
					val html = it?.body()?.string()
					it?.close()
					return@map html
				}

		return parser.parseProfileFromPage(page)
	}

	fun filter(profiles: Flux<Profile>): Flux<Profile> {
		log.debug("filter")
		return profiles.filter {
			val birthdayThisYear = DateUtil.convertToBirthdayInThisYear(it.birthday)
			val daysUntilBirthday = DateUtil.daysToEvent(birthdayThisYear)

			daysUntilBirthday in 0..30 || daysUntilBirthday in -30..-1
		}
	}

	private fun closeAsynRequest() {
		log.debug("closeAsynRequest")
		httpController
				.client
				.callShutdown()
	}
}