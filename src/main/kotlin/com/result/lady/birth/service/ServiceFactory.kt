package com.result.lady.birth.service

import com.result.lady.birth.http.controller.VictoriaHttpController
import com.result.lady.birth.model.AdminPanel

object ServiceFactory {

    fun createService(panel: AdminPanel): VictoriaService {
        val client = ClientManager.getClient(panel.name)
        val httpController = VictoriaHttpController(client)
        return VictoriaService(panel, httpController)
    }
}