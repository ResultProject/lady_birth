package com.result.lady.birth.service

import com.result.lady.birth.model.AdminPanel
import org.apache.logging.log4j.LogManager
import java.io.*


object AdminPanelStorageService {

	private val log = LogManager.getLogger()

	var adminPanels: MutableList<AdminPanel> = mutableListOf()

	private val filePath = System.getProperty("user.home") + "/.vb/panel"

	init {
		val dir = File(filePath)
		if (!dir.exists()) {
			dir.mkdirs()
		}
	}

	fun save(panels: List<AdminPanel>) {
		log.info("save")
		panels.forEach { panel ->
			val saveFile = File(filePath + "/" + panel.name + ".save")
			if (saveFile.exists())
				saveFile.delete()

			try {
				ObjectOutputStream(FileOutputStream(saveFile)).use { outputStream ->
					outputStream.writeObject(panel)
					outputStream.flush()
				}

				adminPanels.add(panel)
			} catch (e: Exception) {
			}
		}
	}

	fun restore(): List<AdminPanel> {
		log.info("restore")
		val panelSaves = File(filePath).listFiles()

		if (panelSaves.isEmpty())
			return listOf()

		val panels: MutableList<AdminPanel> = mutableListOf()
		panelSaves.forEach { save ->

			try {
				ObjectInputStream(FileInputStream(save)).use { inputStream ->
					val panel = inputStream.readObject() as AdminPanel
					panels.add(panel)
				}
			} catch (e: Exception) {

			}
		}

		adminPanels = panels
		return adminPanels
	}

	fun delete(adminPanel: AdminPanel) {
		log.info("delete ${adminPanel.name}")
		val saveFile = File("$filePath/${adminPanel.name}.save")
		if (saveFile.exists())
			saveFile.delete()

		adminPanels.remove(adminPanel)
	}

	fun delete(panels: List<AdminPanel>) {
		panels.forEach { delete(it) }
	}
}